# name of binary(s)
B_MAIN=vcf2csv

# source dir
D_SRC=src

# bin dir to install to
TARGET=/usr/local/bin

help:
	@echo "make targets:"
	@echo "all      compile all sources"
	@echo "clean    clean up"
	@echo "help     what do you think you are reading?"
	@echo "install  install binaries to system, root requiered"
	@echo "remove   cleanup system, remove binaries"

all:
	make -C $(D_SRC) all

clean:
	make -C $(D_SRC) clean

install: all
	@cp -v $(D_SRC)/$(B_MAIN) $(TARGET)/
	@cat $(D_SRC)/$(B_MAIN).1 | gzip > /usr/share/man/man1/vcf2csv.1.gz

remove:
	@rm -f $(TARGET)/$(B_MAIN)
	@rm -f /usr/share/man/man1/vcf2csv.1.gz
