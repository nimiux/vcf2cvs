/******************************************************************************/
#define VERSION "0.6"

/******************************************************************************/
#define MAX_BUF        1024
#define MAX_LINE      65535
#define MAX_DROP_WORD  1024

/******************************************************************************/
#define TRUE     0 == 0
#define FALSE    0 == 1

/******************************************************************************/
/* FILENAME_MAX: from stdio.h                                                 */
/******************************************************************************/
#define MAX_FN  sizeof(char) * FILENAME_MAX + 1

/******************************************************************************/
#define RST_VALUE        01
#define RST_TYPE         02
/******************************************************************************/
#define OUT_CSV          01
#define OUT_WEB          02

/******************************************************************************/
extern char msg[MAX_BUF];
extern char *program_name;
extern char *input_file_name;
extern char *output_file_name;

extern char dropvalues[MAX_LINE+1];

/******************************************************************************/
typedef struct vcard_st {
  char key[MAX_BUF];
  char val[MAX_LINE];
  struct vcard_st *next;
} vcard;

/******************************************************************************/
/* a list of defined types to ignore is stored in the drop list               */
/* a drop value is a string of a type                                         */
/* these drop values are stored inside a conatenated list                     */
/******************************************************************************/
typedef struct drop_st {
  char key[MAX_DROP_WORD+1];
  struct drop_st *next;
} drop;
